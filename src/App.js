import React, { useState } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Filter from "./component/Filter";
import List from "./component/List";
import Edit from "./component/Edit";

function App() {
  const [Players, setPlayers] = useState([
    {
      id : "1",
      username: "johnmayer",
      email: "johnmayer@gmail.com",
      experience: "700",
      level: "17",
    },
    {
      id : "2",
      username: "chester",
      email: "chester@gmail.com",
      experience: "400",
      level: "44",
    },
    {
      id : "3",
      username: "chris",
      email: "chris@yahoo.com",
      experience: "500",
      level: "66",
    },
  ]);

  const [lastId, setLastId] = useState("3");
  const [filtereddata, setFiltereddata] = useState(Players);
  const [editing, setEditing] = useState(false);
  const [currentdata, setCurrentData] = useState({
    id : "",
    username: "",
    email: "",
    experience: "",
    level: "",
  });
  const filterHandler = (filtered) => {
    if (
      !filtered.username &&
      !filtered.email &&
      !filtered.experience &&
      !filtered.level
    ) {
      setFiltereddata(Players);
      return;
    }

    const filters = Players.filter((players) => {
      if (filtered.username && players.username !== filtered.username)
        return false;
      if (filtered.email && players.email !== filtered.email) return false;
      if (filtered.experience && players.experience !== filtered.experience)
        return false;
      if (filtered.level && players.level !== filtered.level) return false;
      return true;
    });
    setFiltereddata(filters);
  };

  const newPlayer = (data) => {
    const id = lastId + 1;
    const newdata = {
    id : id,
    username: data.username,
    email: data.email,
    experience: data.experience,
    level: data.level,
    }
    setPlayers([...Players, newdata]);
    setFiltereddata([...Players, newdata]);
    setLastId(id)
    setCurrentData({
      id : "",
      username: "",
      email: "",
      experience: "",
      level: "",
    })
  };

  const editData = (data) => {
    setEditing(true);
    setCurrentData({
      id : data.id,
      username: data.username,
      email: data.email,
      experience: data.experience,
      level: data.level,
    });
  };

  const updateData = (id, updatedData) => {
    setEditing(false);
    setFiltereddata(
      filtereddata.map((item) => (item.id === id ? updatedData : item))
    );
    setPlayers(
      Players.map((item) => (item.id === id ? updatedData : item))
    );
  };

  return (
    <div>
      <Container className="pt-5">
        <Row>
          <h1 className="text-center">Player</h1>
          {editing ? 
        (<Edit
          setEditing={setEditing}
          editing={editing}
          updateData={updateData}
          currentdata={currentdata}
        />) : (<Filter onFilter={filterHandler} />)}
          <List data={filtereddata} add={newPlayer} editData={editData} currentdata={currentdata} />
        </Row>
      </Container>
    </div>
  );
}

export default App;
