import React, { useState } from "react";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

export default function Filter(props){
    const [fields, setFields] = useState({
        username : "",
        email : "",
        level : "",
        experience : "",
    })

    const handleInputChange = event => {
      const { name, value } = event.target
      
      setFields({ ...fields, [name]: value })
      }
      
    return(
        <Col sm={2} className="pt-5 border border-secondary rounded">
          <h2>Filter</h2>
          <div className="mb-3">
          <label className="form-label">Username</label>
          <input className="form-control" placeholder="Enter Username" value={fields.username} name="username" onChange={handleInputChange}/>
        </div>
        <div className="mb-3">
          <label className="form-label">Email</label>
          <input className="form-control" placeholder="Enter Email" name="email" onChange={handleInputChange}/>
        </div>
        <div className="mb-3">
          <label className="form-label">Experience</label>
          <input className="form-control" placeholder="Enter Experience" name="experience" onChange={handleInputChange}/>
        </div>
        <div className="mb-3">
          <label className="form-label">Level</label>
          <input className="form-control" placeholder="Enter Level" name="level" onChange={handleInputChange}/>
        </div>
        <Button className="m-3" variant="primary" onClick={()=>props.onFilter(fields)}>
            Submit
          </Button>
          
        </Col>
    )
    }