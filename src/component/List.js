import React, { useState } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

export default function List(props) {
  const [addplayer, setAddplayer] = useState("");
  const [fields, setFields] = useState({id : "",
  username: "",
  email: "",
  experience: "",
  level: "",
});

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setFields({ ...fields, [name]: value });
  };

  const handlesubmit = () => {
    setFields("")
    setAddplayer("")
  }
  return (
    <Col sm={10}>
      <Row>
        <Col>
          <div className="d-flex justify-content-end">
            {addplayer === "tambah" ? (
              <form onSubmit={handlesubmit} className="flex">
                <div className="m-3">
                  <label className="form-label">Username</label>
                  <input
                    className="form-control"
                    value={fields.username}
                    placeholder="Enter Username"
                    name="username"
                    onChange={handleInputChange}
                  />
                </div>
                <div className="m-3">
                  <label className="form-label">Email</label>
                  <input
                    className="form-control"
                    value={fields.email}
                    placeholder="Enter Email"
                    name="email"
                    onChange={handleInputChange}
                  />
                </div>
                <div className="m-3">
                  <label className="form-label">Experience</label>
                  <input
                    className="form-control"
                    value={fields.experience}
                    placeholder="Enter Experience"
                    name="experience"
                    onChange={handleInputChange}
                  />
                </div>
                <div className="m-3">
                  <label className="form-label">Level</label>
                  <input
                    className="form-control"
                    value={fields.level}
                    placeholder="Enter Level"
                    name="level"
                    onChange={handleInputChange}
                  />
                </div>
                <Button
                  type="submit"
                  onClick={() => props.add(fields)}
                  className="m-4"
                  variant="primary"
                  disabled={!fields.username ,!fields.email , !fields.experience , !fields.level}
                >
                  Submit
                </Button>
              </form>
            ) : (
              <Button
                onClick={() => setAddplayer("tambah")}
                className="m-3"
                variant="primary"
                type="submit"
              >
                Add
              </Button>
            )}
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table striped bordered hover size="sm" className="text-center">
            <thead>
              <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Experience</th>
                <th>Level</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {props.data.map((player) => (
                <tr>
                  <td>{player.username}</td>
                  <td>{player.email}</td>
                  <td>{player.experience}</td>
                  <td>{player.level}</td>
                  <td>
                    <Button
                      onClick={() => {
                        props.editData(player);
                      }}
                      variant="primary"
                      type="submit"
                    >
                      Edit
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Row>
            </Col>
  );
}
