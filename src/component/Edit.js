import React, { useState, useEffect } from "react";
import Col from "react-bootstrap/Col";

const Edit = (props) => {
  const [datadata, setDatadata] = useState(props.currentdata);

  const handleInputEdit = (event) => {
    const { name, value } = event.target;

    setDatadata({ ...datadata, [name]: value });
  };

  return (
    <Col sm="2" className="pt-5 border border-secondary rounded pb-2">
      <h2>Edit Data</h2>
      <form
        onSubmit={(event) => {
          event.preventDefault();
          props.updateData(datadata.id, datadata);
        }}
      >
        <div className="mb-3">
          <label className="form-label">Username</label>
          <input
            type="text"
            name="username"
            className="form-control"
            placeholder="Enter Username"
            value={datadata.username}
            onChange={handleInputEdit}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Email</label>
          <input
            type="text"
            name="email"
            className="form-control"
            placeholder="Enter Email"
            value={datadata.email}
            onChange={handleInputEdit}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Experience</label>
          <input
            type="text"
            name="experience"
            className="form-control"
            placeholder="Enter Experience"
            value={datadata.experience}
            onChange={handleInputEdit}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Level</label>
          <input
            type="text"
            name="level"
            className="form-control"
            placeholder="Enter Level"
            value={datadata.level}
            onChange={handleInputEdit}
          />
        </div>
        <button
          disabled={
            (!datadata.username,
            !datadata.email,
            !datadata.experience,
            !datadata.level)
          }
          className="btn btn-primary"
        >
          Submit
        </button>
        <button
          onClick={() => props.setEditing(false)}
          className="btn btn-primary m-1"
        >
          Cancel
        </button>
      </form>
    </Col>
  );
};
export default Edit;
